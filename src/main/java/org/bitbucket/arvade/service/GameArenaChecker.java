package org.bitbucket.arvade.service;


import org.bitbucket.arvade.model.Round;

public interface GameArenaChecker {
    Round checkForWinningRow(Round[][] arena);
}
