package org.bitbucket.arvade.service.impl;


import com.google.inject.Inject;
import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPresenter;

import java.util.List;

public class GameArenaCheckerImpl implements GameArenaChecker {

    private GameArenaPresenter gameArenaPresenter;

    @Inject
    public GameArenaCheckerImpl(GameArenaPresenter gameArenaPresenter) {
        this.gameArenaPresenter = gameArenaPresenter;
    }

    @Override
    public Round checkForWinningRow(Round[][] arena) {
        List<List<Round>> allRows = this.gameArenaPresenter.getAllRows(arena);

        if (checkFor(allRows, Round.CIRCLE)) {
            return Round.CIRCLE;
        }
        if (checkFor(allRows, Round.CROSS)) {
            return Round.CROSS;
        }

        if (isAllFieldsFilled(allRows)) {
            return Round.FILLED;
        }

        return Round.NONE;
    }

    private boolean isAllFieldsFilled(List<List<Round>> allRows) {
        for (List<Round> row : allRows) {
            for (Round round : row) {
                if (round == Round.NONE) {
                    return false;
                }
            }
        }
        return true;
    }

    private boolean checkFor(List<List<Round>> allRows, Round check) {
        for (List<Round> row : allRows) {
            if (row.stream().allMatch(round -> round == check)) {
                return true;
            }
        }
        return false;
    }
}