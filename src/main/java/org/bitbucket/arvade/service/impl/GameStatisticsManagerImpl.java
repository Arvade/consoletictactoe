package org.bitbucket.arvade.service.impl;

import lombok.Getter;
import lombok.Setter;
import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameStatisticsManager;

public class GameStatisticsManagerImpl implements GameStatisticsManager {

    private static final String SCORE_TEMPLATE = "X: %d, O: %d, To win: %d";

    @Setter
    private String scoreTemplate = SCORE_TEMPLATE;

    @Getter
    private Integer amountOfGamesToWin;
    @Getter
    private Integer circleWinCounter = 0;
    @Getter
    private Integer crossWinCounter = 0;
    @Getter
    private Round whoStarts;

    @Override
    public void printScore() {
        String formattedTemplate = String.format(scoreTemplate, crossWinCounter, circleWinCounter, amountOfGamesToWin);
        System.out.println(formattedTemplate);
    }


    @Override
    public void incrementAmountOfWins(Round winnerOfRound) {
        if (winnerOfRound == Round.CIRCLE) {
            circleWinCounter++;
        } else {
            crossWinCounter++;
        }
    }

    @Override
    public void loadGameConfig(GameConfig gameConfig) {
        this.whoStarts = gameConfig.getWhoStart();
        this.amountOfGamesToWin = gameConfig.getAmountOfGamesToWin();
    }
}
