package org.bitbucket.arvade.service.impl;

import org.bitbucket.arvade.model.GameArenaTemplate;
import org.bitbucket.arvade.service.GameArenaPrinter;

import javax.inject.Inject;

public class GameArenaPrinterImpl implements GameArenaPrinter {

    private GameArenaTemplate gameArenaTemplate;

    @Inject
    public GameArenaPrinterImpl(GameArenaTemplate gameArenaTemplate) {
        this.gameArenaTemplate = gameArenaTemplate;
    }

    @Override
    public void printArena(Object[] arena) {
        String arenaTemplate  = gameArenaTemplate.getArenaTemplate();

        String formattedTemplate = String.format(arenaTemplate, arena);
        System.out.println(formattedTemplate);
    }
}