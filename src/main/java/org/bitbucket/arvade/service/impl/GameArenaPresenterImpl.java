package org.bitbucket.arvade.service.impl;


import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameArenaPresenter;

import java.util.ArrayList;
import java.util.List;

public class GameArenaPresenterImpl implements GameArenaPresenter {

    @Override
    public List<Round> getFirstHorizontalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][0]);
        outputRow.add(arena[0][1]);
        outputRow.add(arena[0][2]);

        return outputRow;
    }

    @Override
    public List<Round> getSecondHorizontalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[1][0]);
        outputRow.add(arena[1][1]);
        outputRow.add(arena[1][2]);

        return outputRow;
    }

    @Override
    public List<Round> getThirdHorizontalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[2][0]);
        outputRow.add(arena[2][1]);
        outputRow.add(arena[2][2]);

        return outputRow;
    }

    @Override
    public List<Round> getFirstVerticalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][0]);
        outputRow.add(arena[1][0]);
        outputRow.add(arena[2][0]);

        return outputRow;
    }

    @Override
    public List<Round> getSecondVerticalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][1]);
        outputRow.add(arena[1][1]);
        outputRow.add(arena[2][1]);

        return outputRow;
    }

    @Override
    public List<Round> getThirdVerticalRow(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][2]);
        outputRow.add(arena[1][2]);
        outputRow.add(arena[2][2]);

        return outputRow;
    }

    @Override
    public List<Round> getFirstDiagonal(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][0]);
        outputRow.add(arena[1][1]);
        outputRow.add(arena[2][2]);

        return outputRow;
    }

    @Override
    public List<Round> getSecondDiagonal(Round[][] arena) {
        List<Round> outputRow = new ArrayList<>();

        outputRow.add(arena[0][2]);
        outputRow.add(arena[1][1]);
        outputRow.add(arena[2][0]);

        return outputRow;
    }

    @Override
    public List<List<Round>> getAllRows(Round[][] arena) {
        List<List<Round>> outputRow = new ArrayList<>();

        outputRow.add(getFirstVerticalRow(arena));
        outputRow.add(getSecondVerticalRow(arena));
        outputRow.add(getThirdVerticalRow(arena));
        outputRow.add(getFirstHorizontalRow(arena));
        outputRow.add(getSecondHorizontalRow(arena));
        outputRow.add(getThirdHorizontalRow(arena));
        outputRow.add(getFirstDiagonal(arena));
        outputRow.add(getSecondDiagonal(arena));

        return outputRow;
    }
}
