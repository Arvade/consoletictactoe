package org.bitbucket.arvade.service;

import org.bitbucket.arvade.model.Round;

import java.util.List;

public interface GameArenaPresenter {
    List<Round> getFirstHorizontalRow(Round[][] arena);

    List<Round> getSecondHorizontalRow(Round[][] arena);

    List<Round> getThirdHorizontalRow(Round[][] arena);

    List<Round> getFirstVerticalRow(Round[][] arena);

    List<Round> getSecondVerticalRow(Round[][] arena);

    List<Round> getThirdVerticalRow(Round[][] arena);

    List<Round> getFirstDiagonal(Round[][] arena);

    List<Round> getSecondDiagonal(Round[][] arena);

    List<List<Round>> getAllRows(Round[][] arena);
}
