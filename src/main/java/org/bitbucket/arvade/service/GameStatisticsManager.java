package org.bitbucket.arvade.service;

import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;

public interface GameStatisticsManager {
    Integer getCrossWinCounter();

    Integer getCircleWinCounter();

    Integer getAmountOfGamesToWin();

    Round getWhoStarts();

    void incrementAmountOfWins(Round winner);

    void loadGameConfig(GameConfig gameConfig);

    void printScore();
}