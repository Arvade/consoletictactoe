package org.bitbucket.arvade.service;

public interface GameArenaPrinter {

    void printArena(Object[] arena);
}