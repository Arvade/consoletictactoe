package org.bitbucket.arvade;

import com.google.inject.AbstractModule;
import org.bitbucket.arvade.factory.GameContextFactory;
import org.bitbucket.arvade.factory.impl.GameContextFactoryImpl;
import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.GameArenaTemplate;
import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.provider.GameArenaProvider;
import org.bitbucket.arvade.provider.GameArenaTemplateProvider;
import org.bitbucket.arvade.provider.GameConfigProvider;
import org.bitbucket.arvade.provider.GameStatisticsManagerProvider;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPresenter;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;
import org.bitbucket.arvade.service.impl.GameArenaCheckerImpl;
import org.bitbucket.arvade.service.impl.GameArenaPresenterImpl;
import org.bitbucket.arvade.service.impl.GameArenaPrinterImpl;

public class MainModule extends AbstractModule {

    @Override
    protected void configure() {
        bind(GameArenaChecker.class).to(GameArenaCheckerImpl.class);
        bind(GameArenaPrinter.class).to(GameArenaPrinterImpl.class);
        bind(GameArenaPresenter.class).to(GameArenaPresenterImpl.class);
        bind(GameContextFactory.class).to(GameContextFactoryImpl.class);
        bind(GameStatisticsManager.class).toProvider(GameStatisticsManagerProvider.class);
        bind(GameConfig.class).toProvider(GameConfigProvider.class);
        bind(GameArena.class).toProvider(GameArenaProvider.class);
        bind(GameArenaTemplate.class).toProvider(GameArenaTemplateProvider.class).asEagerSingleton();
    }
}