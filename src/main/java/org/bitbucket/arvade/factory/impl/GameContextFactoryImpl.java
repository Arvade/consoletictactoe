package org.bitbucket.arvade.factory.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.factory.GameContextFactory;
import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.impl.GameContext;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;

public class GameContextFactoryImpl implements GameContextFactory {

    private final GameArenaPrinter gameArenaPrinter;
    private final GameArenaChecker gameArenaChecker;
    private final GameStatisticsManager gameStatisticsManager;
    private final GameArena gameArena;

    @Inject
    public GameContextFactoryImpl(GameArenaPrinter gameArenaPrinter,
                                  GameArenaChecker gameArenaChecker,
                                  GameStatisticsManager gameStatisticsManager, GameArena gameArena) {
        this.gameArenaPrinter = gameArenaPrinter;
        this.gameArenaChecker = gameArenaChecker;
        this.gameStatisticsManager = gameStatisticsManager;
        this.gameArena = gameArena;
    }

    @Override
    public GameContext create() {
        return new GameContext(gameArenaPrinter, gameArenaChecker, gameStatisticsManager, gameArena);
    }
}
