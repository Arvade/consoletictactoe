package org.bitbucket.arvade.factory;

import org.bitbucket.arvade.model.impl.GameContext;

public interface GameContextFactory {
    GameContext create();
}
