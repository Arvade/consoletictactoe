package org.bitbucket.arvade;

import com.google.inject.Guice;
import com.google.inject.Injector;
import org.bitbucket.arvade.factory.GameContextFactory;
import org.bitbucket.arvade.model.impl.GameContext;


public class Main {
    private Injector injector;

    public void init() {
        MainModule mainModule = new MainModule();
        this.injector = Guice.createInjector(mainModule);

        GameContextFactory gameContextFactory = this.injector.getInstance(GameContextFactory.class);
        GameContext gameContext = gameContextFactory.create();
        gameContext.startGame();
    }

    public static void main(String[] args) {
        new Main().init();
    }

}