package org.bitbucket.arvade.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.GameArenaTemplate;

public class GameArenaProvider implements Provider<GameArena> {

    private GameArenaTemplate gameArenaTemplate;

    @Inject
    public GameArenaProvider(GameArenaTemplate gameArenaTemplate) {
        this.gameArenaTemplate = gameArenaTemplate;
    }

    @Override
    public GameArena get() {
        return new GameArena(gameArenaTemplate);
    }
}
