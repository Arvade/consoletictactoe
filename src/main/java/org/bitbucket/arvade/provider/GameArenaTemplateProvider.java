package org.bitbucket.arvade.provider;

import com.google.inject.Provider;
import lombok.Setter;
import org.bitbucket.arvade.model.GameArenaTemplate;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameArenaTemplateProvider implements Provider<GameArenaTemplate> {

    private static final Logger logger = Logger.getLogger(GameArenaTemplateProvider.class.getName());
    private static final String ARENA_TEMPLATE_FILE = "arena_template.txt";

    @Setter
    private String arenaTemplateFile = ARENA_TEMPLATE_FILE;

    private boolean isLoaded = false;
    private GameArenaTemplate gameArenaTemplate = new GameArenaTemplate();

    @Override
    public GameArenaTemplate get() {
        if (isLoaded) {
            return gameArenaTemplate;
        }

        StringBuilder arenaTemplate = new StringBuilder();
        Optional<InputStream> inputStreamOptional = readFile();

        if (inputStreamOptional.isPresent()) {
            try (BufferedReader br = new BufferedReader(new InputStreamReader(inputStreamOptional.get()))) {
                String line;
                while ((line = br.readLine()) != null) {
                    arenaTemplate.append(line).append("\n");
                }
            } catch (IOException e) {
                logger.log(Level.INFO, "There was a problem while loading a configuration file.");
            }
            gameArenaTemplate.setArenaTemplate(arenaTemplate.toString());
        } else {
            arenaTemplate.append("%s | %s | %s\n")
                    .append("%s | %s | %s\n")
                    .append("%s | %s | %s\n");

            gameArenaTemplate.setArenaTemplate(arenaTemplate.toString());
        }
        isLoaded = true;

        return gameArenaTemplate;
    }

    private Optional<InputStream> readFile() {
        return Optional.ofNullable(getClass().getResourceAsStream("/" + arenaTemplateFile));
    }
}
