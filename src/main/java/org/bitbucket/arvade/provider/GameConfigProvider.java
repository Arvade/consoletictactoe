package org.bitbucket.arvade.provider;

import com.google.inject.Provider;
import lombok.Setter;
import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GameConfigProvider implements Provider<GameConfig> {

    private static final Logger LOGGER = Logger.getLogger(GameConfigProvider.class.getName());
    private static final String DEFAULT_CONFIG_FILENAME = "/game_config.config";

    @Setter
    private String defaultConfigFilename = DEFAULT_CONFIG_FILENAME;

    @Override
    public GameConfig get() {
        LOGGER.log(Level.INFO, "Loading game configuration...");

        GameConfig gameConfig = new GameConfig();
        Properties properties = new Properties();

        try {
            InputStream resourceAsStream = getClass().getResourceAsStream(defaultConfigFilename);
            if (resourceAsStream == null) {
                LOGGER.log(Level.INFO, () -> "Configuration file '" + defaultConfigFilename + "' not found.");
                LOGGER.log(Level.INFO, () -> "Loading defaults...");
            } else {
                LOGGER.log(Level.INFO, "Found configuration file...");
                properties.load(new InputStreamReader(resourceAsStream));

                Integer amountOfGamesToWin = Integer.parseInt((String) properties.getOrDefault("amountOfGamesToWin", gameConfig.getAmountOfGamesToWin()));
                Round whoStarts = Round.valueOf((String) properties.getOrDefault("whoStarts", gameConfig.getWhoStart().toString()));

                gameConfig.setWhoStart(whoStarts);
                gameConfig.setAmountOfGamesToWin(amountOfGamesToWin);
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }

        LOGGER.log(Level.INFO, () -> "Loaded default game configuration: \n" +
                "\tStarts: " + gameConfig.getWhoStart() +
                "\n\tAmount of games to win: " + gameConfig.getAmountOfGamesToWin());
        //TODO: Refactor to clean way of loading configuration files once.
        return gameConfig;
    }
}
