package org.bitbucket.arvade.provider;

import com.google.inject.Inject;
import com.google.inject.Provider;
import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.service.GameStatisticsManager;
import org.bitbucket.arvade.service.impl.GameStatisticsManagerImpl;

public class GameStatisticsManagerProvider implements Provider<GameStatisticsManager> {

    private GameConfig gameConfig;

    @Inject
    public GameStatisticsManagerProvider(GameConfig gameConfig) {
        this.gameConfig = gameConfig;
    }

    @Override
    public GameStatisticsManager get() {
        GameStatisticsManager gameStatisticsManager = new GameStatisticsManagerImpl();
        gameStatisticsManager.loadGameConfig(gameConfig);

        return gameStatisticsManager;
    }
}