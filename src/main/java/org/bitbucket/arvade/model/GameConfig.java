package org.bitbucket.arvade.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GameConfig {

    private Integer amountOfGamesToWin = 1;

    private Round whoStart = Round.CIRCLE;

    public GameConfig(Round whoStart) {
        this.whoStart = whoStart;
    }
}
