package org.bitbucket.arvade.model;

import java.util.ArrayList;
import java.util.List;

public class GameArena {

    private Round[][] arena = new Round[3][3];
    private GameArenaTemplate gameArenaTemplate;

    public GameArena(GameArenaTemplate gameArenaTemplate) {
        resetArena();
        this.gameArenaTemplate = gameArenaTemplate;
    }

    public void resetArena() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                arena[i][j] = Round.NONE;
            }
        }
    }

    public boolean mark(int x, int y, Round mark) {
        if (x > 2 || y > 2) {
            return false;
        }
        if (x < 0 || y < 0) {
            return false;
        }

        if (arena[x][y] == Round.NONE) {
            arena[x][y] = mark;
            return true;
        } else {
            return false;
        }
    }

    public Object[] getArenaAsOneDimensionalArray() {
        List<Round> list = new ArrayList<>();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                list.add(arena[i][j]);
            }
        }
        return list.toArray();
    }

    public Round[][] getArena() {
        return this.arena;
    }

    public String toString() {
        return String.format(gameArenaTemplate.getArenaTemplate(), getArenaAsOneDimensionalArray());
    }
}