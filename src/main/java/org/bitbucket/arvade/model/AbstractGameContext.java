package org.bitbucket.arvade.model;


import com.google.inject.Inject;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;

import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class AbstractGameContext {

    private static final Logger LOGGER = Logger.getLogger(AbstractGameContext.class.getName());

    protected GameStatisticsManager gameStatisticsManager;
    protected GameArenaPrinter gameArenaPrinter;
    protected GameArenaChecker gameArenaChecker;
    protected GameArena gameArena;
    protected Round currentRound;

    private boolean isGameOn = false;

    @Inject
    public AbstractGameContext(GameArenaPrinter gameArenaPrinter,
                               GameArenaChecker gameArenaChecker,
                               GameStatisticsManager gameStatisticsManager,
                               GameArena gameArena) {
        this.gameArenaPrinter = gameArenaPrinter;
        this.gameArenaChecker = gameArenaChecker;
        this.gameStatisticsManager = gameStatisticsManager;
        this.gameArena = gameArena;
    }

    public void startGame() {
        loadStartupConfig();
        this.isGameOn = true;
        processGame();
    }

    private void processGame() {
        while (isGameOn) {
            printArena();
            printRound();
            move();
            clearConsole();

            Round checkResult = checkForWinningRow();
            if (checkResult != Round.NONE) {
                winRound(checkResult);
            }
            changeRound();
        }
    }

    private void clearConsole() {
        for (int i = 0; i < 20; i++) {
            System.out.println();
        }
    }

    private void winRound(Round winner) {
        if (winner == Round.FILLED) {
            System.out.println("No one wins this round!");
            resetGame();
            printScore();
            return;
        }
        System.out.println(winner + " wins this round!");
        gameStatisticsManager.incrementAmountOfWins(winner);

        Round checkResult = checkIfOneOfPlayersHaveEnoughOfWins();
        if (checkResult != Round.NONE) {
            System.out.println(checkResult + " wins this game!");
            if (!askForNewGame()) {
                endGame();
            }
        }

        resetGame();
        printScore();
    }

    private void endGame() {
        this.isGameOn = false;
        System.out.println("Thanks for playing!");
        try {
            Thread.sleep(200);
        } catch (InterruptedException e) {
            LOGGER.log(Level.WARNING, e.getMessage());
            Thread.currentThread().interrupt();
        }
        System.exit(0);
    }

    private boolean askForNewGame() {
        System.out.println("Would you like to play again? (Y/n)");
        Scanner scanner = new Scanner(System.in);
        String answer;
        do {
            answer = scanner.next();
        } while (!(answer.equalsIgnoreCase("y")) && !(answer.equalsIgnoreCase("n")) || answer.equalsIgnoreCase(""));

        return !answer.equalsIgnoreCase("n");
    }

    protected abstract void move();

    protected abstract void resetGame();

    protected abstract void printRound();

    protected abstract void printScore();

    protected abstract void printArena();

    protected abstract void changeRound();

    protected abstract void loadStartupConfig();

    protected abstract Round checkForWinningRow();

    protected abstract Round checkIfOneOfPlayersHaveEnoughOfWins();
}