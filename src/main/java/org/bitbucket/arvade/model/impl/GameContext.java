package org.bitbucket.arvade.model.impl;

import com.google.inject.Inject;
import org.bitbucket.arvade.model.AbstractGameContext;
import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;

import java.util.Scanner;

public class GameContext extends AbstractGameContext {

    @Inject
    public GameContext(GameArenaPrinter gameArenaPrinter,
                       GameArenaChecker gameArenaChecker,
                       GameStatisticsManager gameStatisticsManager,
                       GameArena gameArena) {
        super(gameArenaPrinter, gameArenaChecker, gameStatisticsManager, gameArena);
    }

    @Override
    protected Round checkIfOneOfPlayersHaveEnoughOfWins() {
        Integer circleWinCounter = gameStatisticsManager.getCircleWinCounter();
        Integer crossWinCounter = gameStatisticsManager.getCrossWinCounter();
        Integer amountOfGamesToWin = gameStatisticsManager.getAmountOfGamesToWin();

        if (circleWinCounter >= amountOfGamesToWin) {
            return Round.CIRCLE;
        }
        if (crossWinCounter >= amountOfGamesToWin) {
            return Round.CROSS;
        }

        return Round.NONE;
    }

    @Override
    protected void loadStartupConfig() {
        this.currentRound = this.gameStatisticsManager.getWhoStarts();
    }

    @Override
    protected void printArena() {
        this.gameArenaPrinter.printArena(gameArena.getArenaAsOneDimensionalArray());
    }

    @Override
    protected void changeRound() {
        if (currentRound == Round.CIRCLE) {
            currentRound = Round.CROSS;
        } else {
            currentRound = Round.CIRCLE;
        }
    }

    @Override
    protected void move() {
        Scanner scanner = new Scanner(System.in);
        boolean markResult;
        do {
            System.out.println("Podaj wspolrzedne: ");
            System.out.print("x: ");
            int x = scanner.nextInt();
            System.out.print("\ny: ");
            int y = scanner.nextInt();

            markResult = gameArena.mark(y, x, currentRound);
            if (!markResult) {
                System.out.println("You chose wrong place, Try again!");
            }
        } while (!markResult);
    }

    @Override
    protected void printRound() {
        System.out.println("Current round for " + currentRound);
    }

    @Override
    protected Round checkForWinningRow() {
        return gameArenaChecker.checkForWinningRow(gameArena.getArena());
    }

    @Override
    protected void resetGame() {
        this.gameArena.resetArena();
    }

    @Override
    protected void printScore() {
        gameStatisticsManager.printScore();
    }
}