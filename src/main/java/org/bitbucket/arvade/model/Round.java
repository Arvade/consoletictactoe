package org.bitbucket.arvade.model;

public enum Round {
    CIRCLE("O"), CROSS("X"), FILLED(""), NONE(" ");

    private final String text;

    Round(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return this.text;
    }
}