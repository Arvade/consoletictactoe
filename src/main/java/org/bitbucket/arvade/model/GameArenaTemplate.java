package org.bitbucket.arvade.model;

import lombok.Data;

@Data
public class GameArenaTemplate {

    private String arenaTemplate;
}
