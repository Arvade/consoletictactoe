package org.bitbucket.arvade.factory.impl;

import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.impl.GameContext;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GameContextFactoryImplTest {

    @Mock
    private GameArenaChecker gameArenaChecker;
    @Mock
    private GameArenaPrinter gameArenaPrinter;
    @Mock
    private GameStatisticsManager gameStatisticsManager;
    @Mock
    private GameArena gameArena;

    @InjectMocks
    private GameContextFactoryImpl gameContextFactory;

    @Test
    public void shouldReturnGameContext() {
        //Given
        GameContext expected = new GameContext(gameArenaPrinter, gameArenaChecker, gameStatisticsManager,gameArena);

        //When
        GameContext result = gameContextFactory.create();

        //Then
        assertThat(result).isEqualToComparingOnlyGivenFields(expected, "gameArenaPrinter",
                "gameArenaChecker", "gameStatisticsManager");
    }
}
