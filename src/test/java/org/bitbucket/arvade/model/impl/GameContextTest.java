package org.bitbucket.arvade.model.impl;

import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameArenaChecker;
import org.bitbucket.arvade.service.GameArenaPrinter;
import org.bitbucket.arvade.service.GameStatisticsManager;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.lang.reflect.Field;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameContextTest {

    @Mock
    private GameArenaPrinter gameArenaPrinter;

    @Mock
    private GameArenaChecker gameArenaChecker;

    @Mock
    private GameStatisticsManager gameStatisticsManager;

    @InjectMocks
    private GameContext gameContext;

    private ByteArrayOutputStream outContent = new ByteArrayOutputStream();

    @Before
    public void setup(){
        System.setOut(new PrintStream(outContent));
    }

    @After
    public void restore(){
        System.setOut(System.out);
    }

    @Test
    public void shouldReturnCircleIfHasEnoughOfWins() {
        //Given
        when(gameStatisticsManager.getAmountOfGamesToWin()).thenReturn(2);
        when(gameStatisticsManager.getCircleWinCounter()).thenReturn(2);
        when(gameStatisticsManager.getCrossWinCounter()).thenReturn(1);
        Round expected = Round.CIRCLE;

        //When
        Round result = gameContext.checkIfOneOfPlayersHaveEnoughOfWins();

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnCrossIfHasEnoughOfWins() {
        //Given
        when(gameStatisticsManager.getAmountOfGamesToWin()).thenReturn(2);
        when(gameStatisticsManager.getCrossWinCounter()).thenReturn(2);
        when(gameStatisticsManager.getCircleWinCounter()).thenReturn(0);
        Round expected = Round.CROSS;

        //When
        Round result = gameContext.checkIfOneOfPlayersHaveEnoughOfWins();

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnNoneIfNoOneHasEnoughOfWins() {
        //Given
        when(gameStatisticsManager.getAmountOfGamesToWin()).thenReturn(3);
        when(gameStatisticsManager.getCircleWinCounter()).thenReturn(2);
        when(gameStatisticsManager.getCrossWinCounter()).thenReturn(2);
        Round expected = Round.NONE;

        //When
        Round result = gameContext.checkIfOneOfPlayersHaveEnoughOfWins();

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldChangeRound() throws NoSuchFieldException, IllegalAccessException {
        //Given
        when(gameStatisticsManager.getWhoStarts()).thenReturn(Round.CIRCLE);
        gameContext.loadStartupConfig();
        Round expected = Round.CROSS;
        Field field = gameContext.getClass().getSuperclass().getDeclaredField("currentRound");
        field.setAccessible(true);

        //When
        gameContext.changeRound();
        Round result = (Round) field.get(gameContext);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldCallPrintScore(){
        //When
        gameContext.printScore();

        //Then
        verify(gameStatisticsManager, times(1)).printScore();
    }

    @Test
    public void shouldPrintRound(){
        //Given
        gameContext.changeRound();
        String expected = "Current round for O";

        //When
        gameContext.printRound();

        //Then
        assertThat(outContent.toString().trim()).isEqualToIgnoringCase(expected);
    }
}

