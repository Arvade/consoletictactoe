package org.bitbucket.arvade.model.impl;

import org.bitbucket.arvade.model.GameArena;
import org.bitbucket.arvade.model.GameArenaTemplate;
import org.bitbucket.arvade.model.Round;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class GameArenaTest {

    private Round[][] cleanArena = new Round[3][3];
    private Round[][] markedArena = new Round[3][3];

    @Mock
    private GameArenaTemplate gameArenaTemplate;

    @InjectMocks
    private GameArena gameArena;

    @Before
    public void setup() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cleanArena[i][j] = Round.NONE;
                markedArena[i][j] = Round.NONE;
            }
        }
        markedArena[1][1] = Round.CROSS;
        when(gameArenaTemplate.getArenaTemplate()).thenCallRealMethod();
    }

    @Test
    public void shouldResetArena() {
        //Given
        gameArena.mark(1, 1, Round.CROSS);

        //When
        gameArena.resetArena();
        Round[][] result = gameArena.getArena();

        //Then

        assertThat(result).isEqualTo(cleanArena);
    }

    @Test
    public void shouldReturnFalseIfWrongCordsProvided() {
        //Given
        int x = 4;
        int y = 1;
        Round mark = Round.CIRCLE;

        //When
        boolean result = gameArena.mark(x, y, mark);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueIfGoodCordsProvided() {
        //Given
        int x = 1;
        int y = 1;
        Round mark = Round.CIRCLE;

        //When
        boolean result = gameArena.mark(x, y, mark);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldMarkGameArenaField() {
        //Given
        int x = 1;
        int y = 1;
        Round mark = Round.CIRCLE;

        //When
        gameArena.mark(x, y, mark);
        Round[][] result = gameArena.getArena();

        //Then
        assertThat(result[x][y]).isEqualTo(mark);
    }

    @Test
    public void shouldReturnArenaasOneDimensionalArray() {
        //Given
        gameArena.mark(1, 1, Round.CROSS);
        Object[] expected = new Object[]{
                Round.NONE, Round.NONE, Round.NONE,
                Round.NONE, Round.CROSS, Round.NONE,
                Round.NONE, Round.NONE, Round.NONE};

        //When
        Object[] result = gameArena.getArenaAsOneDimensionalArray();

        //Then
        assertThat(result).isEqualTo(expected);
    }
}
