package org.bitbucket.arvade.provider.impl;

import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.provider.GameStatisticsManagerProvider;
import org.bitbucket.arvade.service.GameStatisticsManager;
import org.bitbucket.arvade.service.impl.GameStatisticsManagerImpl;
import org.junit.Before;
import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GameStatisticsManagerProviderTest {

    private Round whoStarts = Round.CIRCLE;
    private Integer amountOfGamesToWin = 5;

    private GameConfig gameConfig = new GameConfig(amountOfGamesToWin, whoStarts);

    private GameStatisticsManagerProvider gameStatisticsManagerProvider;

    @Before
    public void setup(){
        this.gameStatisticsManagerProvider = new GameStatisticsManagerProvider(gameConfig);
    }

    @Test
    public void shouldReturnObjectWhenGetCalled(){
        //Given
        GameStatisticsManager expected = new GameStatisticsManagerImpl();
        expected.loadGameConfig(gameConfig);

        //When
        GameStatisticsManager result = gameStatisticsManagerProvider.get();

        //Then
        assertThat(result).isEqualToComparingFieldByField(expected);
    }
}
