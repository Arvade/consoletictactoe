package org.bitbucket.arvade.service.impl;


import org.bitbucket.arvade.model.Round;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class GameArenaPresenterImplTest {

    private GameArenaPresenterImpl gameArenaPresenter;

    private Round[][] arena;


    @Before
    public void setup() {
        arena = new Round[][]{
                {Round.CIRCLE, Round.NONE, Round.CROSS},
                {Round.CROSS, Round.CIRCLE, Round.CROSS},
                {Round.NONE, Round.CIRCLE, Round.NONE},
        };

        gameArenaPresenter = new GameArenaPresenterImpl();
    }

    @Test
    public void shouldReturnFirstHorizontalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CIRCLE);
        expected.add(Round.NONE);
        expected.add(Round.CROSS);

        //When
        List<Round> result = gameArenaPresenter.getFirstHorizontalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnSecondHorizontalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CROSS);
        expected.add(Round.CIRCLE);
        expected.add(Round.CROSS);

        //When
        List<Round> result = gameArenaPresenter.getSecondHorizontalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnThirdHorizontalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.NONE);
        expected.add(Round.CIRCLE);
        expected.add(Round.NONE);

        //When
        List<Round> result = gameArenaPresenter.getThirdHorizontalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnFirstVerticalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CIRCLE);
        expected.add(Round.CROSS);
        expected.add(Round.NONE);

        //When
        List<Round> result = gameArenaPresenter.getFirstVerticalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnSecondVerticalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.NONE);
        expected.add(Round.CIRCLE);
        expected.add(Round.CIRCLE);

        //When
        List<Round> result = gameArenaPresenter.getSecondVerticalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnThirdVerticalRow() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CROSS);
        expected.add(Round.CROSS);
        expected.add(Round.NONE);

        //When
        List<Round> result = gameArenaPresenter.getThirdVerticalRow(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnFirstDiagonal() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CIRCLE);
        expected.add(Round.CIRCLE);
        expected.add(Round.NONE);

        //When
        List<Round> result = gameArenaPresenter.getFirstDiagonal(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldReturnSecondDiagonal() {
        //Given
        List<Round> expected = new ArrayList<>();
        expected.add(Round.CROSS);
        expected.add(Round.CIRCLE);
        expected.add(Round.NONE);

        //When
        List<Round> result = gameArenaPresenter.getSecondDiagonal(arena);

        //Then
        assertThat(result).isEqualTo(expected);
    }

}
