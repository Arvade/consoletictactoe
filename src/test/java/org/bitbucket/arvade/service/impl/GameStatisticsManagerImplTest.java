package org.bitbucket.arvade.service.impl;

import org.bitbucket.arvade.model.GameConfig;
import org.bitbucket.arvade.model.Round;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.assertThat;

public class GameStatisticsManagerImplTest {

    private GameStatisticsManagerImpl gameStatisticsManager = new GameStatisticsManagerImpl();

    private final ByteArrayOutputStream outContent = new ByteArrayOutputStream();
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();


    @Before
    public void setup(){
        System.setOut(new PrintStream(outContent));
        System.setErr(new PrintStream(errContent));
    }

    @After
    public void restoreStreams(){
        System.setOut(System.out);
        System.setErr(System.err);
    }

    @Test
    public void shouldIncrementAmountOfWinsForSpecifiedPlayer(){
        //Given
        Round winnerOfRound = Round.CROSS;
        Integer expected = 1;

        //When
        gameStatisticsManager.incrementAmountOfWins(winnerOfRound);
        Integer result = gameStatisticsManager.getCrossWinCounter();

        //Then
        assertThat(result).isEqualTo(expected);
    }

    @Test
    public void shouldLoadGameConfig(){
        //Given
        GameConfig gameConfig = new GameConfig(3, Round.CIRCLE);
        Integer expectedAmountOfGamesToWin = 3;
        Round expectedWhoStarts = Round.CIRCLE;

        //When
        gameStatisticsManager.loadGameConfig(gameConfig);

        //Then
        assertThat(gameStatisticsManager.getWhoStarts()).isEqualTo(expectedWhoStarts);
        assertThat(gameStatisticsManager.getAmountOfGamesToWin()).isEqualTo(expectedAmountOfGamesToWin);
    }

    @Test
    public void shouldPrintCurrentScore(){
        //Given
        GameConfig gameConfig = new GameConfig(5,Round.CIRCLE);
        gameStatisticsManager.loadGameConfig(gameConfig);
        gameStatisticsManager.incrementAmountOfWins(Round.CROSS);
        gameStatisticsManager.incrementAmountOfWins(Round.CROSS);
        gameStatisticsManager.incrementAmountOfWins(Round.CIRCLE);

        String expected = "X: 2, O: 1, To win: 5";

        //When
        gameStatisticsManager.printScore();

        //Then
        assertThat(outContent.toString().trim()).isEqualTo(expected);
    }
}
