package org.bitbucket.arvade.service.impl;

import org.bitbucket.arvade.model.Round;
import org.bitbucket.arvade.service.GameArenaPresenter;
import org.bitbucket.arvade.service.impl.GameArenaCheckerImpl;
import org.bitbucket.arvade.service.impl.GameArenaPresenterImpl;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@RunWith(MockitoJUnitRunner.class)
public class GameArenaCheckerImplTest {

    private GameArenaCheckerImpl gameArenaChecker;

    @Before
    public void prepare() {
        GameArenaPresenter gameArenaPresenter = new GameArenaPresenterImpl();
        gameArenaChecker = new GameArenaCheckerImpl(gameArenaPresenter);
    }

    @Test
    public void shouldReturnTrueIfAllFieldsAreFilled() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //Given
        Method isAllFieldsFilledMethod = gameArenaChecker.getClass().getDeclaredMethod("isAllFieldsFilled", List.class);
        isAllFieldsFilledMethod.setAccessible(true);

        List<List<Round>> allRows = new ArrayList<>();
        Round[] rowAsArray = new Round[5];
        Arrays.fill(rowAsArray, Round.CROSS);
        List<Round> row = Arrays.asList(rowAsArray);
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));

        //When
        boolean result = (boolean) isAllFieldsFilledMethod.invoke(gameArenaChecker, allRows);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfNotAllFieldsAreFilled() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        //Given
        Method isAllFieldFilledMethod= gameArenaChecker.getClass().getDeclaredMethod("isAllFieldsFilled", List.class);
        isAllFieldFilledMethod.setAccessible(true);
        List<List<Round>> allRows = new ArrayList<>();
        Round[] rowAsArray = new Round[5];
        Arrays.fill(rowAsArray,Round.CIRCLE);
        List<Round> row = Arrays.asList(rowAsArray);
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        row.set(0, Round.NONE);
        allRows.add(new ArrayList<>(row));

        //When
        boolean result = (boolean) isAllFieldFilledMethod.invoke(gameArenaChecker, allRows);

        //Then
        assertThat(result).isFalse();
    }

    @Test
    public void shouldReturnTrueIfRowExistsWithSameElementsExists() throws NoSuchMethodException, InvocationTargetException, IllegalAccessException {
        //Given
        Method checkForMethod = gameArenaChecker.getClass().getDeclaredMethod("checkFor", List.class, Round.class);
        checkForMethod.setAccessible(true);
        Round wanted = Round.CIRCLE;

        List<List<Round>> allRows = new ArrayList<>();
        Round[] rowAsArray =  new Round[6];
        Arrays.fill(rowAsArray, Round.NONE);

        List<Round> row = Arrays.asList(rowAsArray);
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        Arrays.fill(rowAsArray, Round.CIRCLE);
        allRows.add(new ArrayList<>(Arrays.asList(rowAsArray)));

        //When
        boolean result = (boolean) checkForMethod.invoke(gameArenaChecker, allRows, wanted);

        //Then
        assertThat(result).isTrue();
    }

    @Test
    public void shouldReturnFalseIfNoRowExistsWithSameElement() throws InvocationTargetException, IllegalAccessException, NoSuchMethodException {
        //Given
        Method checkForMethod = gameArenaChecker.getClass().getDeclaredMethod("checkFor", List.class, Round.class);
        checkForMethod.setAccessible(true);
        Round wanted = Round.CROSS;

        List<List<Round>> allRows = new ArrayList<>();
        Round[] rowAsArray =  new Round[5];
        Arrays.fill(rowAsArray, Round.NONE);

        List<Round> row = Arrays.asList(rowAsArray);
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));
        allRows.add(new ArrayList<>(row));

        //When
        boolean result = (boolean) checkForMethod.invoke(gameArenaChecker, allRows, wanted);

        //Then
        assertThat(result).isFalse();
    }
}
